import requests
import pandas as pd
import pytz
from ..config import balkash_collection
from concurrent.futures import ThreadPoolExecutor
import asyncio
from datetime import datetime, timedelta

class MLService:
    METEO_attributes = [
        'Irradiation',
        'Temperature_panel',
        'Dew_point_C',
        'Precipitation_mm/hour',
        'Air_Pressure_hPa',
        'Brightness_Candela/m2',
        'Humidity_%',
        'Total Power',
        'Wind_speed_m/s',
        'Precipitation'
    ]

    BLOCKS_attributes = [
        'Irradiation',
        'Temperature_panel',
        'a1',
        'a2',
        'a3',
        'a4',
        'a5',
        'a6',
        'a7',
        'a8',
        'power forecast'
    ]

    file_path = 'src/files/ml_data/ml_data.xlsx'

    def __init__(self):
        self.executor = ThreadPoolExecutor()

    async def fetch_and_process_weather_data(self):
        api_url = 'https://api.3elabs.eu/solarforecast?latitude=46.941222&longitude=75.001417&inclination=30&azimuth=180&convention=forward&variables=global_horizontal,global_inclined,ambient_temperature,relative_humidity,wind_speed&resolution=h&authorization=XKaZp5w2Qf2bR5e27XnWQ4zxTYx0Q5S03UGdZhw0'
        response = await asyncio.to_thread(requests.get, api_url)
        if response.status_code == 200:
            weather_data = response.json()
            print(weather_data)
        else:
            print('Failed to retrieve weather data. Status code:', response.status_code)

        weather_df = pd.DataFrame(weather_data['data'], columns=weather_data['columns'],
                                  index=pd.to_datetime(weather_data['index']))
        weather_df = weather_df.rename_axis("datetime").reset_index()

        utc_timezone = pytz.timezone('UTC')
        utc_plus_6_timezone = pytz.timezone('Asia/Almaty')

        # Convert 'datetime' to the Asia/Almaty timezone without converting to string
        weather_df['datetime_KZ'] = weather_df['datetime'].apply(lambda x: x.astimezone(utc_plus_6_timezone))

        # Optionally format the 'datetime' column as a string if needed
        weather_df['datetime'] = weather_df['datetime'].dt.strftime('%Y-%m-%d %H:%M:%S')

        # If datetime_KZ should remain as datetime type, do not convert it to string
        # weather_df['datetime_KZ'] = weather_df['datetime_KZ'].dt.strftime('%Y-%m-%d %H:%M:%S')

        # Fix column assignment (it seems there was a mistake in forming a list of lists)
        new_columns = ['datetime', 'Irradiation', 'inclined_irradiation', 'Air_Temp', 'Humidity_%', 'Wind_speed_m/s',
                       'datetime_KZ']
        weather_df.columns = new_columns

        # Handle NaN values
        weather_df.fillna(0, inplace=True)

        dataset_final = pd.read_excel('src/files/data.xlsx', index_col=0)

        weather_df['date'] = weather_df['datetime_KZ'].dt.date
        weather_df['time'] = weather_df['datetime_KZ'].dt.time

        dataset_final['date'] = dataset_final['Datetime'].dt.date
        dataset_final['time'] = dataset_final['Datetime'].dt.time

        def calculate_power_forecast(dataset_final, weather_df):
            power_forecasts = []

            # Iterate through each forecast entry
            for index, row in weather_df.iterrows():
                forecast_time = row['time']
                forecast_date = row['date']
                forecast_irradiation = row['Irradiation']

                # Filter historical data for the same hour and the most recent 20 entries before the forecast date
                filtered_history = dataset_final[
                    (dataset_final['time'] == forecast_time) & (dataset_final['date'] < forecast_date)]
                filtered_history = filtered_history.sort_values(by='date', ascending=False).head(20)

                # Find the 5 closest irradiation values
                closest_entries = filtered_history.iloc[
                    (filtered_history['Irradiation'] - forecast_irradiation).abs().argsort()[:5]]

                # Calculate the average power of these entries
                average_power = closest_entries['Power'].mean()

                # Append the result
                power_forecasts.append(average_power)

            weather_df['power forecast'] = power_forecasts
            return weather_df

        weather_df = calculate_power_forecast(dataset_final, weather_df)
        print(weather_df[['datetime', 'power forecast']])

        weather_df['datetime_KZ'] = weather_df['datetime_KZ'].dt.strftime('%Y-%m-%d %H:%M:%S')
        weather_df.to_excel('src/files/ml_data/ml_data.xlsx')

    async def insert_prediction_data_in_certain_day(self, future_date):
        excel_data = pd.read_excel(self.file_path)
        year = future_date.year
        month = f'{future_date.month:02}'
        day = f'{future_date.day:02}'
        timestamp = f'{year}-{month}-{day}'
        data = {
            timestamp: {
                'intech': {}
            }
        }

        for hour in range(24):
            data[timestamp]['intech'][str(hour)] = {}

        for index, row in excel_data.iterrows():
            METEO_d = {}
            BLOCKS_d = {}
            hour = int(row['time'][:2])
            data[timestamp]['intech'][str(hour)] = {
                "BLOCKS": BLOCKS_d,
                "METEO": METEO_d
            }
            for column in excel_data.columns:
                if column == 'Unnamed: 0':
                    continue
                if column == 'datetime':
                    if timestamp != row[column][:10]:
                        continue
                if column in self.METEO_attributes:
                    METEO_d[column] = row[column]
                if column in self.BLOCKS_attributes:
                    if column == 'power forecast':
                        BLOCKS_d['power'] = row[column]
                        continue
                    BLOCKS_d[column] = row[column]

        await balkash_collection.insert_one(data)

    async def insert_ml_data_into_mongo(self):
        today = datetime.today()
        day_of_week = today.strftime('%A')
        if day_of_week == 'Saturday' or day_of_week == 'Sunday':
            return
        if day_of_week == 'Friday':
            await self.insert_prediction_data_in_certain_day(today + timedelta(days=3))
            await self.insert_prediction_data_in_certain_day(today + timedelta(days=4))
        await self.insert_prediction_data_in_certain_day(today + timedelta(days=2))

    async def generate_excel_for_ml(self):
        today = datetime.today()
        new_excel_data = {
            'Total Power': [],
            'Humidity_%': [],
            'Air_Pressure_hPa': [],
            'Temperature_panel': [],
            'Dew_point_C': [],
            'Brightness_Candela/m2': [],
            'Wind_speed_m/s': [],
            'Precipitation': [],
            'a1': [],
            'a2': [],
            'a3': [],
            'a4': [],
            'a5': [],
            'a6': [],
            'a7': [],
            'a8': [],
            'Precipitation_mm/hour': [],
            'Irradiation': [],
            'Air_Temp': [],
            'Datetime': [],
            'Power': []
        }
        date_format = "%Y-%m-%d %H:%M:%S"

        cursor = balkash_collection.find()
        documents = await cursor.to_list(None)

        for document in documents:
            date = None
            for key, value in document.items():
                if key != '_id':
                    date = key
            if datetime.strptime(date, "%Y-%m-%d").date() >= today.date():
                continue
            if 'actual' not in document[date]:
                continue
            actual_data = document[date]['actual']
            for hour in range(24):
                date_string = f'{date} {hour}:00:00'
                date_obj = datetime.strptime(date_string, date_format)
                new_excel_data['Datetime'].append(date_obj)
            for hour in range(24):
                for field in actual_data[str(hour)]['BLOCKS']:
                    new_excel_data[field].append(actual_data[str(hour)]['BLOCKS'][field])
            for hour in range(24):
                for field in actual_data[str(hour)]['METEO']:
                    if field == 'Irradiation' or field == 'Air_Temp':
                        continue
                    new_excel_data[field].append(actual_data[str(hour)]['METEO'][field])

        df = pd.DataFrame(new_excel_data)

        df.to_excel('src/files/data.xlsx', index=True)


mlService = MLService()