import pandas as pd
from datetime import datetime
import re
from ..config import balkash_collection


class BlocksService:
    async def insert_blocks_into_mongo(self):
        file_path = 'src/files/yesterday_files/Blocks.xlsx'

        excel_data = pd.read_excel(file_path, skiprows=4, skipfooter=4)

        ru_to_en = {
            'Блок А1\nкВтч.': 'a1',
            'Блок А2\nкВтч.': 'a2',
            'Блок А3\nкВтч.': 'a3',
            'Блок А4\nкВтч.': 'a4',
            'Блок А5\nкВтч.': 'a5',
            'Блок А6\nкВтч.': 'a6',
            'Блок А7\nкВтч.': 'a7',
            'Блок А8\nкВтч.': 'a8',
            'Иррадиация\nВт./м2': 'Irradiation',
            'Температура \nВоздуха С': 'Air_Temp',
        }
        day = None
        month = None
        d = {}
        for index, row in excel_data.iterrows():
            inner_d = {}
            d[str(index)] = {
                'BLOCKS': inner_d
            }
            power = 0
            for column in excel_data.columns:
                if column == 'Время':
                    if day == None and month == None:
                        day = row[column][:2]
                        month = row[column][3:5]
                    continue
                inner_d[ru_to_en[column]] = row[column]
                if re.match(r"a\d+", ru_to_en[column]):
                    power += float(row[column])
            d[str(index)]['BLOCKS']['Power'] = power / 1000

        year = datetime.now().year
        timestamp = f'{year}-{month}-{day}'

        query = {timestamp: {"$exists": True}}

        document_count = await balkash_collection.count_documents(query)

        if document_count == 0:
            record = {
                timestamp: {
                    'actual': d
                }
            }
            await balkash_collection.insert_one(record)
        else:
            record = await balkash_collection.find_one(query)
            if 'actual' not in record[timestamp]:
                record[timestamp]['actual'] = {}
            for i in range(24):
                if str(i) not in record[timestamp]['actual']:
                    record[timestamp]['actual'][str(i)] = {}
                record[timestamp]['actual'][str(i)]['BLOCKS'] = d[str(i)]['BLOCKS']
            await balkash_collection.update_one(query, {"$set": record})

blocksService = BlocksService()