from ..config import balkash_collection, almaty_tz
from datetime import datetime
import random
import aiohttp

class FakeDataGenerationService:

    def random_percent_variation(self, number, percent_range=40):
        if number == 0:
            return 0
        # Calculate the maximum variation
        max_variation = number * (percent_range / 100.0)

        # Generate a random variation within the range
        variation = random.uniform(-max_variation, max_variation * 0.8)

        # Apply the variation to the original number
        new_number = number + variation

        return new_number

    async def fetch(self, endpoint):
        async with aiohttp.ClientSession() as session:
            async with session.get(endpoint) as response:
                return await response.json()

    async def generateData(self):
        current_time = datetime.now(almaty_tz)
        current_timestamp = current_time.strftime('%Y-%m-%d')
        current_hour = int(current_time.strftime('%H'))

        initial_actual_data = {}

        for hour in range(0, 24):
            hour_data = {
                'METEO': {
                    'Temperature_panel': 0,
                    'Irradiation': 0,
                    'Air_Temp': 0
                },
                'BLOCKS': {
                    'Power': 0
                }
            }
            initial_actual_data[str(hour)] = hour_data

        query_actual = {current_timestamp: {"$exists": True}}

        record = await balkash_collection.find_one(query_actual)

        if 'actual' not in record[current_timestamp]:
            record[current_timestamp]['actual'] = initial_actual_data
            await balkash_collection.update_one(query_actual, {"$set": record})

        intech_power = record[current_timestamp]['intech'][str(current_hour)]['BLOCKS']['power']
        new_power = abs(self.random_percent_variation(intech_power))
        record[current_timestamp]['actual'][str(current_hour)]['BLOCKS']['Power'] = new_power

        current_meteo_data = await self.fetch('http://192.168.1.12:8000/api/v1/trends/components/192')
        if current_hour < 6 or current_hour > 20:
            await balkash_collection.update_one(query_actual, {"$set": record})
        temperature = 0
        irradiation = 0
        FEM_temperature = 0
        for item in current_meteo_data:
            if item['description'] == 'Температура окружающей среды С':
                temperature = item['v']
            if item['description'] == 'Солнечная иррадиация':
                irradiation = item['v']
            if item['description'] == 'Температура ФЭМ':
                FEM_temperature = item['v']
        record[current_timestamp]['actual'][str(current_hour)]['METEO']['Irradiation'] = irradiation
        record[current_timestamp]['actual'][str(current_hour)]['METEO']['Air_Temp'] = temperature
        record[current_timestamp]['actual'][str(current_hour)]['METEO']['Temperature_panel'] = FEM_temperature
        await balkash_collection.update_one(query_actual, {"$set": record})

fakeDataGenerationService = FakeDataGenerationService()
