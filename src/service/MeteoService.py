import pandas as pd
from datetime import datetime
from ..config import balkash_collection


class MeteoService:
    async def insert_meteo_into_mongo(self):
        file_path = 'src/files/yesterday_files/Meteo.xlsx'

        excel_data = pd.read_excel(file_path, skiprows=3, skipfooter=3)

        ru_to_en = {
            'Иррадиация\nВт./м2.': 'Irradiation',
            'Температура\nВоздуха °С': 'Air_Temp',
            'Температура\nФЭМ °C': 'Temperature_panel',
            'Точка \nРосы °С': 'Dew_point_C',
            'Осадки кол-во\nмм/час.': 'Precipitation_mm/hour',
            'Давление\nhPa': 'Air_Pressure_hPa',
            'Яркость \nкд/м2': 'Brightness_Candela/m2',
            'Влажность\n %': 'Humidity_%',
            'Мощность \nАктив. Мвтч.': 'Total Power',
            'Скорость \nветра м/с.': 'Wind_speed_m/s',
            'Осадки\nТип': 'Precipitation'
        }
        day = None
        month = None
        d = {}
        for index, row in excel_data.iterrows():
            inner_d = {}
            d[str(index)] = {
                'METEO': inner_d
            }
            for column in excel_data.columns:
                if column == 'Время':
                    if day == None and month == None:
                        day = row[column][:2]
                        month = row[column][3:5]
                    continue
                if column not in ru_to_en:
                    continue
                inner_d[ru_to_en[column]] = row[column]

        year = datetime.now().year
        timestamp = f'{year}-{month}-{day}'

        query = {timestamp: {"$exists": True}}

        document_count = await balkash_collection.count_documents(query)

        if document_count == 0:
            record = {
                timestamp: {
                    'actual': d
                }
            }
            await balkash_collection.insert_one(record)
        else:
            record = await balkash_collection.find_one(query)
            if 'actual' not in record[timestamp]:
                record[timestamp]['actual'] = {}
            for i in range(24):
                if str(i) not in record[timestamp]['actual']:
                    record[timestamp]['actual'][str(i)] = {}
                record[timestamp]['actual'][str(i)]['METEO'] = d[str(i)]['METEO']
            await balkash_collection.update_one(query, {"$set": record})

meteoService = MeteoService()