from fastapi import HTTPException
from ..config import balkash_collection

class BlocksCrudService:

    async def get_blocks_day_stats(self, timestamp):
        document = await balkash_collection.find_one({timestamp: {"$exists": True}})
        if document == None:
            raise HTTPException(status_code=404, detail="Item not found")

        result = [
            {
                'name' : 'Actual',
                'series' : []
            },
            {
                'name': 'Intech',
                'series': []
            }
        ]

        for hour in range(24):
            record = {
                "name": hour,
                "value": "0.00"
            }
            if 'actual' in document[timestamp]:
                if str(hour) not in document[timestamp]['actual']:
                    continue
                record["value"] = f"{document[timestamp]['actual'][str(hour)]['BLOCKS']['Power']:.2f}"
            result[0]['series'].append(record)
        for hour in range(24):
            record = {
                "name": hour,
                "value": "0.00"
            }
            if 'intech' in document[timestamp]:
                record["value"] = f"{document[timestamp]['intech'][str(hour)]['BLOCKS']['power']:.2f}"
            result[1]['series'].append(record)

        return result

    async def get_blocks_month_stats(self, timestamp):
        cursor = balkash_collection.find({})
        documents = {}

        year = timestamp[:4]
        month = timestamp[5:7]

        async for document in cursor:
            for key, value in document.items():
                if key.startswith(timestamp):
                    documents[key] = value

        if (len(documents) == 0):
            raise HTTPException(status_code=404, detail="Item not found")

        result = [
            {
                'name': 'Actual',
                'series': []
            },
            {
                'name': 'Intech',
                'series': []
            }
        ]

        days_28 = ['02']

        days_30 = ['04', '06', '09', '11']

        days_31 = ['01', '03', '05', '07', '08', '10', '12']

        number_of_days = 0

        if month in days_28:
            number_of_days = 28
        if month in days_30:
            number_of_days = 30
        if month in days_31:
            number_of_days = 31

        for day in range(1, number_of_days + 1):
            record = {
                'name': day,
                'value': "0.00"
            }
            date = f"{year}-{month}-{day:02}"
            if date in documents:
                if 'actual' in documents[date]:
                    power = 0.00
                    for hour in range(24):
                        power += documents[date]['actual'][str(hour)]['BLOCKS']['Power']
                    record['value'] = f"{power:.2f}"
            result[0]['series'].append(record)

        for day in range(1, number_of_days + 1):
            record = {
                'name': day,
                'value': "0.00"
            }
            date = f"{year}-{month}-{day:02}"
            if date in documents:
                if 'intech' in documents[date]:
                    power = 0.00
                    for hour in range(24):
                        power += documents[date]['intech'][str(hour)]['BLOCKS']['power']
                    record['value'] = f"{power:.2f}"
            result[1]['series'].append(record)

        return result

    async def get_blocks_year_stats(self, timestamp):
        cursor = balkash_collection.find({})
        documents = {}

        year = timestamp

        async for document in cursor:
            for key, value in document.items():
                if key.startswith(timestamp):
                    documents[key] = value

        result = [
            {
                'name': 'Actual',
                'series': []
            },
            {
                'name': 'Intech',
                'series': []
            }
        ]

        for month in range(1, 13):
            record = {
                'name': month,
                'value': "0.00"
            }
            date = f'{year}-{month:02}'
            power = 0.00
            for key in documents.keys():
                if key.startswith(date):
                    if 'actual' in documents[key]:
                        for hour in range(24):
                            power += documents[key]['actual'][str(hour)]['BLOCKS']['Power']
            record['value'] = f"{power:.2f}"
            result[0]['series'].append(record)

        for month in range(1, 13):
            record = {
                'name': month,
                'value': "0.00"
            }
            date = f'{year}-{month:02}'
            power = 0.00
            for key in documents.keys():
                if key.startswith(date):
                    if 'intech' in documents[key]:
                        for hour in range(24):
                            power += documents[key]['intech'][str(hour)]['BLOCKS']['power']
            record['value'] = f"{power:.2f}"
            result[1]['series'].append(record)



        return result






blocksCrudService = BlocksCrudService()