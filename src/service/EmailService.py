import imaplib
import email
from email.header import decode_header
import os
import shutil

class EmailService:
    def __list_latest_messages(self, imap_server, username, password, num_messages=50):
        mail = imaplib.IMAP4_SSL(imap_server)
        mail.login(username, password)

        mail.select("inbox")

        status, messages = mail.search(None, "ALL")
        email_ids = messages[0].split()

        latest_email_ids = email_ids[-num_messages:]
        for email_id in reversed(latest_email_ids):
            status, msg_data = mail.fetch(email_id, "(RFC822)")
            msg = email.message_from_bytes(msg_data[0][1])

            from_header = msg.get("From")
            if from_header:
                sender = decode_header(from_header)[0][0]
                if isinstance(sender, bytes):
                    sender = sender.decode()
                if sender == 'SesBalkhash@tgs-energy.kz':
                    self.__download_attachments(msg)
                    mail.logout()
                    return

        mail.logout()

    def __delete_all_files_in_directory(self, directory):
        for filename in os.listdir(directory):
            file_path = os.path.join(directory, filename)
            try:
                if os.path.isfile(file_path) or os.path.islink(file_path):
                    os.unlink(file_path)
                elif os.path.isdir(file_path):
                    shutil.rmtree(file_path)
                print("src/files/yesterday_files are cleaned")
            except Exception as e:
                print(f'Failed to delete {file_path}. Reason: {e}')

    def __download_attachments(self, msg):
        attachment_dir = os.path.join(os.getcwd(), "src/files/yesterday_files")
        if not os.path.exists(attachment_dir):
            os.makedirs(attachment_dir)
        attachment_count = 0

        for part in msg.walk():
            if part.get_content_maintype() == 'multipart':
                continue
            if part.get('Content-Disposition') is None:
                continue

            filename = self.__decode_mime_words(part.get_filename())
            if filename and (filename.endswith(".xlsx") or filename.endswith(".xls")):
                filename = decode_header(filename)[0][0]
                if isinstance(filename, bytes):
                    filename = filename.decode()
                new_filename = None
                if 'Метео' in filename:
                    new_filename = 'Meteo.xlsx'
                if 'Блок' in filename:
                    new_filename = 'Blocks.xlsx'
                filepath = os.path.join(attachment_dir, new_filename)
                with open(filepath, "wb") as f:
                    f.write(part.get_payload(decode=True))
                print(f"Downloaded {filename} to {filepath}")
                attachment_count += 1

                if attachment_count == 2:
                    break

    def __decode_mime_words(self, s):
        return ''.join(
            word.decode(encoding or 'utf8') if isinstance(word, bytes) else word
            for word, encoding in email.header.decode_header(s)
        )

    def get_yesterday_result(self):
        imap_server = 'imap.yandex.com'
        username = 'shyndaulet@tgs-energy.kz'
        password = 'wnsrgleizfgegfhg'

        self.__list_latest_messages(imap_server, username, password, num_messages=50)

emailService = EmailService()
