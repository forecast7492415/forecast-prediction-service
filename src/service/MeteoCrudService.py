from fastapi import HTTPException
from ..config import balkash_collection

class MeteoCrudService:
    async def get_meteo_day_stats(self, timestamp):
        document = await balkash_collection.find_one({timestamp: {"$exists": True}})
        if document == None:
            raise HTTPException(status_code=404, detail="Item not found")
        result = []

        for hour in range(24):
            record = {
                'labels': hour,
                'cloudiness': {
                    'actual': None,
                    'prediction': None
                },
                'irradiance': {
                    'actual': None,
                    'prediction': None
                },
                'precipitation': {
                    'actual': None,
                    'prediction': None
                },
                'temperature': {
                    'actual': None,
                    'prediction': None
                },
                'wind_speed': {
                    'actual': None,
                    'prediction': None
                },
                'temperature_panel': {
                    'actual': None,
                    'prediction': None
                }

            }
            if 'actual' in document[timestamp]:
                record['irradiance']['actual'] = f"{document[timestamp]['actual'][str(hour)]['METEO']['Irradiation']:.2f}"
                # record['precipitation']['actual'] = f"{document[timestamp]['actual'][str(hour)]['METEO']['Precipitation']:.2f}"
                record['temperature']['actual'] = f"{document[timestamp]['actual'][str(hour)]['METEO']['Air_Temp']:.2f}"
                # record['wind_speed']['actual'] = f"{document[timestamp]['actual'][str(hour)]['METEO']['Wind_speed_m/s']:.2f}"
                record['temperature_panel']['actual'] = f"{document[timestamp]['actual'][str(hour)]['METEO']['Temperature_panel']:.2f}"
            if 'intech' in document[timestamp]:
                record['irradiance']['prediction'] = f"{document[timestamp]['intech'][str(hour)]['METEO']['Irradiation']:.2f}"
                # record['wind_speed']['prediction'] = f"{document[timestamp]['intech'][str(hour)]['METEO']['Wind_speed_m/s']:.2f}"
            result.append(record)

        return result


meteoCrudService = MeteoCrudService()