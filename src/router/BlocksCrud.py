from fastapi import APIRouter
from ..service import blocksCrudService

router = APIRouter()

@router.get("/day-stats")
async def get_blocks_day_stats(timestamp):
    result = await blocksCrudService.get_blocks_day_stats(timestamp)
    return result

@router.get("/month-stats")
async def get_blocks_month_stats(timestamp):
    result = await blocksCrudService.get_blocks_month_stats(timestamp)
    return result

@router.get("/year-stats")
async def get_blocks_month_stats(timestamp):
    result = await blocksCrudService.get_blocks_year_stats(timestamp)
    return result