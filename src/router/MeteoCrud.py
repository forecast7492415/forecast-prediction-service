from fastapi import APIRouter
from ..service import meteoCrudService

router = APIRouter()

@router.get("/day-stats")
async def get_meteo_day_stats(timestamp):
    result = await meteoCrudService.get_meteo_day_stats(timestamp)
    return result

