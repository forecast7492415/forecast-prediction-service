from fastapi import FastAPI
from apscheduler.schedulers.asyncio import AsyncIOScheduler
from fastapi.middleware.cors import CORSMiddleware
from apscheduler.triggers.cron import CronTrigger
from .service import emailService, mlService, blocksService, meteoService, fakeDataGenerationService
from datetime import datetime
import asyncio
from .router import BlocksCrud, MeteoCrud

app = FastAPI()
app.include_router(BlocksCrud.router, prefix = '/power-plant')
app.include_router(MeteoCrud.router, prefix = '/meteo')

origins = [
    "*"
]


app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)


scheduler = AsyncIOScheduler()

async def execute_with_delay(coro):
    await coro
    await asyncio.sleep(120)


async def add_intech_data():
    await execute_with_delay(mlService.generate_excel_for_ml())
    await execute_with_delay(mlService.fetch_and_process_weather_data())
    await execute_with_delay(mlService.insert_ml_data_into_mongo())
    print("Task executed at", datetime.now())

async def add_actual_data():
    emailService.get_yesterday_result()
    await asyncio.sleep(120)
    await execute_with_delay(blocksService.insert_blocks_into_mongo())
    await execute_with_delay(meteoService.insert_meteo_into_mongo())
    print("Task executed at", datetime.now())

@app.on_event("startup")
async def startup_event():
    for hour in range(0,24):
        hour_trigger = CronTrigger(hour = hour, minute=5)
        scheduler.add_job(fakeDataGenerationService.generateData, hour_trigger)
    email_trigger = CronTrigger(hour=1, minute=45)
    scheduler.add_job(add_actual_data, trigger=email_trigger)
    forecast_trigger = CronTrigger(hour=9, minute=0)
    scheduler.add_job(add_intech_data, trigger = forecast_trigger)
    scheduler.start()

@app.on_event("shutdown")
async def shutdown_event():
    scheduler.shutdown()

@app.get("/test")
async def read_root():
    # emailService.get_yesterday_result()
    # await blocksService.insert_blocks_into_mongo()
    # await meteoService.insert_meteo_into_mongo()
    # await mlService.generate_excel_for_ml()
    # await mlService.fetch_and_process_weather_data()
    # await mlService.insert_ml_data_into_mongo()
    await fakeDataGenerationService.generateData()
    return {
        "Success" : "true"
    }
