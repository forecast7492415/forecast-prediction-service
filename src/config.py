from pydantic_settings import BaseSettings
from pydantic import MongoDsn

from motor.motor_asyncio import AsyncIOMotorClient

import pytz


class Settings(BaseSettings):
    mongo_url : MongoDsn
    mongo_db : str
    mongo_collection: str

settings = Settings()

client = AsyncIOMotorClient(str(settings.mongo_url))

balkash_db = client[settings.mongo_db]
balkash_collection = balkash_db[settings.mongo_collection]

almaty_tz = pytz.timezone('Asia/Almaty')
